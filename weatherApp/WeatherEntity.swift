//
//  WeatherEntity.swift
//  weatherApp
//
//  Created by Daniyal on 29.01.2021.
//  Copyright © 2021 Daniyal. All rights reserved.
//

import Foundation

class WeatherEntity: Decodable {
    let name: String
    let main: Main
    let weather: [Weather]
    
    class Main: Decodable {
        let temp: Double
    }
    
    class Weather: Decodable {
        let id: Int
    }
}
